package com.example;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.resource.PackageResourceReference;

import de.agilecoders.wicket.extensions.markup.html.bootstrap.icon.FontAwesomeCssReference;

public class HomePage extends WebPage
{
    private static final long serialVersionUID = 1L;

    public HomePage(final PageParameters parameters)
    {
        super(parameters);

        add(new Label("bannedLabel", "Customer is banned"));
        add(new Label("allowedLabel", "Customer is active"));
        add(new Label("version", getApplication().getFrameworkSettings().getVersion()));
    }

    @Override
    public void renderHead(IHeaderResponse response)
    {
        super.renderHead(response);

        //CssHeaderItem fontAwesom = CssHeaderItem.forUrl("css/font-awesome.css");
        //response.render(fontAwesom);

        response.render(CssHeaderItem.forReference(FontAwesomeCssReference.instance()));

        PackageResourceReference cssFile = new PackageResourceReference
                (HomePage.class, "HomePage.css");
        CssHeaderItem cssItem = CssHeaderItem.forReference(cssFile);
        response.render(cssItem);
    }
}
